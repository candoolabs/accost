package Accost;

use lib "$ENV{'HOME'}/perl5/lib/perl5";
use LWP::UserAgent;
use JSON -support_by_pp;
use FindBin qw($Bin);
use File::Basename;
use Data::Dumper;

# This will hold all data
$data = {};
$etherscan = "http://api.etherscan.io/api?module=account&action=txlist&address=_ACCOUNT_&startblock=0&endblock=99999999&sort=asc&apikey=W5F5TE4SMZMMS71EU7UCHAB8SIRFJ846BM";
$etherscan_internal = "http://api.etherscan.io/api?module=account&action=txlistinternal&address=_ACCOUNT_&startblock=0&endblock=99999999&sort=asc&apikey=W5F5TE4SMZMMS71EU7UCHAB8SIRFJ846BM";
$etherscanTx = "https://api.etherscan.io/api?module=proxy&action=eth_getTransactionByHash&txhash=_TX_&apikey=W5F5TE4SMZMMS71EU7UCHAB8SIRFJ846BM";
$coinbase = "https://api.coinbase.com/v2/prices/ETH-USD/spot?date=_DATE_";
$coinbaseCHF = "https://api.coinbase.com/v2/prices/ETH-CHF/spot?date=_DATE_";
$output = "$Bin/../data/" . basename($0,(".pl"));

sub go {
    my $config = shift;
    if ($config->{'account'}) {
        # Account mode
        printListByAccount($config->{'account'});
        return;
    }
    if ($config->{'transaction'}) {
        printListByTx($config->{'transaction'});
        return;
    }
    print "Nothing to do\n";
}

sub printListByTx {
    my $tran = shift;
    my $txInfo = getTransaction($tran);
    my $list = getListByAccount($txInfo->{'result'}{'from'});
    foreach my $list (@$list) {
        next unless (lc($tran) eq lc($list->[4]));
        print join(";",@$list) . "\n";
    }
}

sub getListByAccount {
    my $account = shift;
    my $retlist = ();
    my $costs = getTransactionsCBD($account);
    foreach my $cost (@$costs) {
        push(@$retlist,$cost)
    }
    return $retlist;
}

sub printListByAccount {
    my $account = shift;
    my $list = getListByAccount($account);
    foreach my $tlist (@$list) {
        # Mark in and out transactions
        (lc($account) eq lc($tlist->[2])) ? push(@$tlist,"OUT") : push(@$tlist,"IN");
        print join(";",@$tlist) . "\n";
    }
}

sub verifyParams {
    my $config = shift;
    return (
            (exists $config->{'account'}) && 
            ($config->{'account'} =~ /^0x[[:alnum:]]+$/)
           );
}

# Get transactions with costs by transaction date
sub getTransactionsCBD {
    my $account = shift;
    my $transactions = getTransactions($account);
    # This is not possible/not understood at the moment
    # my $itransactions = getITransactions($account);
    # push(@{$transactions->{'result'}},@{$itransactions->{'result'}});
    return retCostsByDate($transactions);
}

sub retCostsByDate {
    my $transactions = shift;
    my $result = ();
    foreach my $tran (@{$transactions->{'result'}}) {
        push(@$result,costs($tran));
    }
    return $result;
}

sub costs {
    my $transaction = shift;
    return collectCostsByTranWPrice($transaction, getRate($transaction));
}

sub getTransaction {
    my $tran = shift;
    $etherscanTx =~ s/_TX_/$tran/;
    my $lbc = LWP::UserAgent->new(ssl_opts => { verify_hostname => 0 });
    my $got = $lbc->get($etherscanTx);
    if ($got->is_success) {
        my $content = decode_json($got->decoded_content);
        return $content;
    } else {
        die $got->status_line;
    }
}

# getTransactions($account)
sub getTransactions {
    my $account = shift;
    $etherscan =~ s/_ACCOUNT_/$account/;
    my $lbc = LWP::UserAgent->new(ssl_opts => { verify_hostname => 0 });
    my $got = $lbc->get($etherscan);
    if ($got->is_success) {
        my $content = decode_json($got->decoded_content);
        return $content;
        # $data->{'transactions'} = $content;
    } else {
        die $got->status_line;
    }
}

# getITransactions($account)
sub getITransactions {
    my $account = shift;
    $etherscan_internal =~ s/_ACCOUNT_/$account/;
    my $lbc = LWP::UserAgent->new(ssl_opts => { verify_hostname => 0 });
    my $got = $lbc->get($etherscan_internal);
    if ($got->is_success) {
        my $content = decode_json($got->decoded_content);
        getGasPrices($content);
        return $content;
        # $data->{'transactions'} = $content;
    } else {
        die $got->status_line;
    }
}

# Gas price is not delivered for internal transactions, so we have to get it separately
sub getGasPrices {
    my $list = shift;
    for (my $i = 0; $i < @{$list->{'result'}}; $i++) {
        unless ($list->{'result'}[$i]{'gasPrice'}) {
            my $tInfo = getTransaction($list->{'result'}[$i]{'hash'});
            $list->{'result'}[$i]{'gasPrice'} = hex($tInfo->{'result'}{'gasPrice'});
            # Gas price is always 0, so we need to change that also
            $list->{'result'}[$i]{'gas'} = hex($tInfo->{'result'}{'gas'});
        }
    }

}

# reportTransactionCost($account)
sub reportTransactionCost {
    my $account = shift;
    foreach my $tran (@{$data->{'transactions'}{'result'}}) {
        my @record = (); # out,cost
        (lc($tran->{'from'}) eq lc($account)) ? push(@record,1) : push(@record,0);
        push(@record,collectCostsByTran($tran));
        $data->{'costs'}{$tran->{'hash'}} = \@record;
    }
}

sub sumIt {
    sumPecunia(0);
    sumPecunia(1);
}

sub sumPecunia {
    my $idx = shift;
    my $sum = 0;
    foreach my $t (keys %{$data->{'costs'}}) {
        $sum += $data->{'costs'}{$t}[1][$idx] if ($data->{'costs'}{$t}[0]);
    }
    $data->{'sum'}[$idx] = $sum;
}

sub printJson {
    my $out = shift;
    open(OUT,">$out") || (warn "Cannot create output file $out: $!" && return);
    print OUT encode_json($data);
    close OUT || warn "Cannot close output file $out: $!";
}

sub printCSV {
    my $out = shift;
    my $config = shift;
    open(OUT,">$out") || (warn "Cannot create output file $out: $!" && return);
    print OUT csvHeader($config);
    print OUT csvTransactions();
    print OUT csvSum();
    close OUT || warn "Cannot close output file $out: $!";
}

sub csvHeader {
    my $config = shift;
    return "Costs of outgoing transaction for account $config->{'account'} per date $config->{'date'}\n\n";
}

sub csvTransactions {
    my $retval = "List of Transactions\n\nHash;Direction;Cost in Ether;Cost in Dollar\n";

    foreach my $tran (keys %{$data->{'costs'}}) {
        $retval .= $tran . ";" . (($data->{'costs'}{$tran}[0]) ? "OUT" : "IN") . ";" . $data->{'costs'}{$tran}[1][0] . ";" . $data->{'costs'}{$tran}[1][1] . "\n";
    }
    $retval .= "\n\n";
    return $retval;
}

sub csvSum {
    my $retval = "Summary\n\n;;Sum OUT in Ether;Sum OUT in Dollar\n;;";
    $retval .= $data->{'sum'}[0] . ";" . $data->{'sum'}[1];
}

sub collectCostsByTranWPrice {
    my $t = shift;
    my $rate = shift;
    my $ethCost = $t->{'value'}/(1e+18);
    if ($t->{'isError'} ne "0") {
        $ethCost = 0;
    }
    my $tranCost = ($t->{'gasUsed'} * $t->{'gasPrice'})/(1e+18);
    return [
	"Ether",
	createDate($t->{'timeStamp'}),
	$t->{'from'},
	$t->{'to'},
	$t->{'hash'},
	$tranCost,
	$ethCost, 
	sprintf("%.2f",$tranCost * $rate->[0]), 
	sprintf("%.2f",$tranCost * $rate->[1]), 
	sprintf("%.2f",$ethCost * $rate->[0]), 
	sprintf("%.2f",$ethCost * $rate->[1]), 
	"?"
    ];
}

sub getRate {
    my $t = shift;
    return retEtherPriceByTimeStamp($t->{'timeStamp'});
}

sub collectCostsByTran {
    my $t = shift;
    return collectCostsByTranWPrice($t,$data->{'rate'});
}

sub retEtherPriceByTimeStamp {
    my $ts = shift;
    my $date = createDate($ts);
    return [retEtherPriceByDate($date,$coinbase),retEtherPriceByDate($date,$coinbaseCHF)]; 
}

sub createDate {
    my $ts = shift;
    my @lt = gmtime($ts);
    $lt[4]++;
    $lt[5] += 1900;
    for (my $i = 0; $i < @lt; $i++) {
        $lt[$i] = "0$lt[$i]" if ($lt[$i] < 10);
    }
    return "$lt[5]-$lt[4]-$lt[3]";
}

sub retEtherPriceByDate {
    my $date = shift;
    my $cb = shift;
    my $retval = ();
    $cb =~ s/_DATE_/$date/;
    my $lbc = LWP::UserAgent->new(ssl_opts => { verify_hostname => 0 });
    my $got = $lbc->get($cb);
    if ($got->is_success) {
        my $content = decode_json($got->decoded_content);
        return $content->{'data'}{'amount'};
    } else {
        die $got->status_line;
    }
}

sub getEtherPriceByDate {
    my $date = shift;
    $data->{'rate'} = retEtherPriceByDate($date,$coinbase);
}


# END
1;
