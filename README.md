# Accost Script

## What is does...

Search for a given account its transactions and provide costs of all transactions per date of the transaction in ether, dollars and swiss francs. If you just give one
transaction it lists the information for just that transaction. The latter can take some time because there is no api to directly find this information.

## Prerequisites

- perl
- modules LWP::UserAgent and JSON 

## Call it

```
% perl accost.pl -a account
```

or

```
% perl accost.pl -t transaction
```

## Result Set

Ether;_date_;_from_;_to_;_txhash_;_trancost_;_amount in ether_;_trancost_in_dollars_;_transcost_in_chf_;_in dollars_;_in chf_;_description_;_direction_
