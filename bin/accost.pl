#!/usr/bin/perl -w
# v0.1 20180313 VL

use Getopt::Long;
use FindBin qw($Bin);
use lib "$Bin/../lib";

use Accost;

use Data::Dumper;

$config = {
    'account' => 0,
    'transaction' => 0
};

&main;


sub main {
    # Get the options
    # account and tx are mutually exclusive
    GetOptions( 
        "account|a=s" => \$config->{'account'},
        "tx|t=s" => \$config->{'transaction'}
    ) || die "Error in command line parsing";

    Accost::go($config);
    
}
